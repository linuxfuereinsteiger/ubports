# VPN Konfigurieren mit der VPN-Editor app

Viele Menschen benutzen VPNs(Virtual Private Networks) um z.B. von außerhalb eines LANs auf die Inhalte dieses(LANs) zugreifen zu können. Dies wird oft verwendet um sich von Zuhause aus, Zugriff auf das Firmennetzwerk zu erlangen um dort Arbeiten zu können.

Unter UT(Ubuntu Touch) kann die Konfiguration auf verschiedene weisen bewerkstelligt werden. Diese Anleitung ist ein Beispiel für die Konfiguration von openVPN mithilfe der [VPN-Editor](https://open-store.io/app/com.ubuntu.developer.pete-woods.vpn-editor) App.

## Getting started

Als erstes, öffnen Sie den `OpenStore` auf Ihren Ubuntu Touch Gerät, suchen nach der App **VPN editor** und Installieren diese.

Für die nächste Schritte benötigen Sie die Anmeldedaten (**OpenVPN/IKEv2 user** und **OpenVPN/IKEv2 password**) und die dazugehörige OpenVPN ServerKonfigurationsdatei `.ovpn`.

Im Fall der Verwendung eines Kommerzielen-Dienstes, melden Sie sich auf die Webseite des Dienstleisters an um die Daten zu erlangen und die **.ovpn**-Datei herunterzuladen.

## CA-Zertifikat und TLS-AuthentifizierungsSchlüssel erstellen

In viele Fälle wird auch ein CA-Zertifikat(**.crt**) und ein TLS-AuthentifizierungsSchlüssel(**.key**) mitgeliefert, der die Konfiguration erleichtert.

Da es nicht unser Fall ist, werden wir in den nächsten Schritten, diese Dateien selbst erstellen. Die nötigen Daten bekommen Sie aus der ServerKonfigurationsdatei `.ovpn`.

### CA-Zertifikat erstellen

Kopieren Sie den Inhalt Ihrer `.ovpn`-Datei unter einen anderen Namen mit der Endung `.crt`, wie z.B.:

```
cp nl-free-23.protonvpn.com.udp.ovpn ProtonVPN_ca.crt
```

Öffnen Sie die neue Datei mit einen Text-Editor:

```
nano ProtonVPN_ca.crt
```

Löschen Sie den Inhalt der neue Datei, so dass nur der CA-Zertifikat übrig bleibt:

```
-----BEGIN CERTIFICATE-----
MIIFozCCA4ugAwIBAgIBATANBgkqhkiG9w0BAQ0FADBAMQswCQYDVQQGEwJDSDEV
MBMGA1UEChMMUHJvdG9uVlBOIEFHMRowGAYDVQQDExFQcm90b25WUE4gUm9vdCBD
QTAeFw0xNzAyMTUxNDM4MDBaFw0yNzAyMTUxNDM4MDBaMEAxCzAJBgNVBAYTAkNI
MRUwEwYDVQQKEwxQcm90b25WUE4gQUcxGjAYBgNVBAMTEVByb3RvblZQTiBSb290

...

Gd7cJ5VkgyycZgLnT9zrChgwjx59JQosFEG1DsaAgHfpEl/N3YPJh68N7fwN41Cj
zsk39v6iZdfuet/sP7oiP5/gLmA/CIPNhdIYxaojbLjFPkftVjVPn49RqwqzJJPR
N8BOyb94yhQ7KO4F3IcLT/y/dsWitY0ZH4lCnAVV/v2YjWAWS3OWyC8BFx/Jmc3W
DK/yPwECUcPgHIeXiRjHnJt0Zcm23O2Q3RphpU+1SO3XixsXpOVOYP6rJIXW9bMZ
A1gTTlpi7A==
-----END CERTIFICATE-----
```
und Speichern diese.

### TLS-AuthentifizierungsSchlüssel erstellen

Kopieren Sie den Inhalt Ihrer `.ovpn`-Datei unter einen anderen Namen, diesmal mit der Endung `.key`, wie z.B.:

```
cp nl-free-23.protonvpn.com.udp.ovpn ProtonVPN_tls-auth.key
```

Öffnen Sie die neue Datei mit einen Text-Editor:

```
nano ProtonVPN_tls-auth.key
```
Löschen Sie den Inhalt der neue Datei, so dass nur der TLS-AuthentifizierungsSchlüssel übrig bleibt:

```
-----BEGIN OpenVPN Static key V1-----
6acef03f62675b4b1bbd03e53b187727
423cea742242106cb2916a8a4c829756
3d22c7e5cef430b1103c6f66eb1fc5b3
75a672f158e2e2e936c3faa48b035a6d

...

2df55785075f37d8c71dc8a42097ee43
344739a0dd48d03025b0450cf1fb5e8c
aeb893d9a96d1f15519bb3c4dcb40ee3
16672ea16c012664f8a9f11255518deb
-----END OpenVPN Static key V1-----
```
und Speichern diese.

## VPN-Client mit der VPN-Editor App konfigurieren

Öffnen Sie Ihre `.ovpn`-Datei und notieren Sie sich die **IP** und Gateway **Port** des Servers, sowie abweichende Einstellungen, um Ihr Client richtig zu Konfigurieren:

```
nano nl-free-23.protonvpn.com.udp.ovpn
```

```
client
dev tun
proto udp

remote 46.166.182.31 443
remote 46.166.182.31 5060
remote 46.166.182.31 4569
remote 46.166.182.31 4569
remote 46.166.182.31 443
remote 46.166.182.31 80
remote 46.166.182.31 443
remote 46.166.182.31 1194
remote 46.166.182.31 443
remote 46.166.182.31 80
remote 46.166.182.31 5060
remote 46.166.182.31 1194
remote 46.166.182.31 4569
remote 46.166.182.31 4569
remote 46.166.182.31 1194
remote 46.166.182.31 1194
remote 46.166.182.31 5060
remote 46.166.182.31 80
remote 46.166.182.31 5060
remote 46.166.182.31 80
server-poll-timeout 20

remote-random
resolv-retry infinite
nobind

# The following setting is only needed for old OpenVPN clients compatibility. New clients
# automatically negotiate the optimal cipher.
cipher AES-256-CBC

auth SHA512
verb 3

setenv CLIENT_CERT 0
tun-mtu 1500
tun-mtu-extra 32
mssfix 1450
persist-key
persist-tun

reneg-sec 0

remote-cert-tls server
auth-user-pass
pull
fast-io

script-security 2
up /etc/openvpn/update-resolv-conf
down /etc/openvpn/update-resolv-conf

...

key-direction 1
```

Dann öffnen Sie die `VPN editor` App und fühlen die Felder mit Ihre Daten, wie im Beispiel:

![editing](editing.png)

![advanced](advanced.png)

![security](security.png)

![tlsauth](tlsauth.png)

Wenn Sie alles richtig ausgefühlt haben, können Sie anschließend die Verbindung zum Server herstellen:

![switchon](switchon.png)

Mit `ip route` können Sie Prüfen ob die Verbindung besteht:

```
ip route
```

![iproute](iproute.png)

## Referenzen

[VPN konfigurieren mit der VPN editor App](https://ubports.com/blog/ubports-blog-1/post/vpn-on-ubuntu-touch-178)
[Wichtige Daten aus der .ovpn um Zertifikate und Schlüssel zu erstellen](https://ubports.com/blog/ubport-blog-1/post/using-vpn-in-ubuntu-touch-132)
