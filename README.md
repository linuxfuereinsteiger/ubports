# UBports

[UBports](https://ubports.com/) ist eine gemeinschaftsgetriebene GNU/Linux-Distribuition, die für den Einsatz auf Smartphones, Tablets und Computer optimiert ist, mit eine einzigartige Touch-Bedienbarkeit die zum verlieben ist, sowie die wunderschöne Grafische Benutzeroberfläche Lomiri (früher Unity8) die auf Qt5.12 basiert.
UBports(UT) ist die, von der Community(Gemeinschaft) weiterentwicklte(portierte) Distribuition Ubuntu Touch, dass früher von Canonical(Ubuntu) selbst entwickelt aber später aufgegeben wurde.

## Getting started

Diese Anleitung hilft Ihnen dabei das Wissen zu erlangen um das Betriebssystem **UBports** auf ihren Smartphone zu installieren. Es wird auch gezeigt wie man Anwendungen/Programme installieren und deinstallieren kann. Die meisten (Terminal-)Befehle in diese Anleitung sind nur auf dem [PinePhone](https://wiki.pine64.org/wiki/PinePhone) getestet worden, dieser ist ein Mainline-Linux Smartphone, der erster seiner Art. Da das PinePhone auch von eine microSD-Speicherkarte booten(starten) kann, ist er perfekt zum rumexperimentieren, da ein kaputtes System einfach wiederhergestellt werden kann, dazu später mehr.

Seien Sie sich bewusst dass bei eine Betriebssysteminstallation alle Daten auf der Festplatte gelöscht werden! Sie können die nächste Schritte auch testweise in eine VM([Virtuelle Maschine](https://de.wikipedia.org/wiki/Virtualisierung_%28Informatik%29)) ausführen.

Sie benötigen für die nächsten Schritte eine leere mindestens 256MB große Speicherkarte um ein Boot-fähiger Installationshilfsgerät zu erstellen, oder eine mind. 8GB große microSD um das Betriebssystem zu brennen. dafür brauchen Sie noch ein Programm wie [Rufus](https://rufus.ie), [balenaEtecher](https://www.balena.io/etcher/), [Win32 Disk Imager](https://sourceforge.net/projects/win32diskimager/) oder auf CD/DVD mit [Brasero](https://wiki.gnome.org/Apps/Brasero), der Ihnen dabei hilft den Startfähigen Installationsgerät zu erstellen.

Für Android-Geräte verwenden Sie bitte den [UBports Installer](https://devices.ubuntu-touch.io/installer/)

Informationen zu unterstüzte Geräte finden Sie [hier](https://ubports.com/de/supported-products).
Alle Ports sind [hier](https://devices.ubuntu-touch.io/) aufgelistet.

Danach geht es weiter mit der [Installation](#installation).

Wenn sie weitere Hilfe/Infos benötigen, haben wir eine Liste weiterführende [Links](#links)

## Hardware Architekturen

* armhf
* arm64
* amd64

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Enable ssh

### Öffentlichen Schlüssel erzeugen (Public Key Verfahren)

Erzeugen Sie einen öffentlichen Schlüssel, falls noch nicht vorhanden. Standarteinstellungen sollten für das LAN ausreichen. Sie können das Kennwortfeld "leer lassen", falls Sie es nicht jedes mal eingeben müssen:

```
ssh-keygen
```

Für eine bessere Verschlüsselung und Kennwortnutzung, können Sie auch einer folgender Beispiele ausprobieren:

```
ssh-keygen -t rsa -b 4096
```

, oder:

```
ssh-keygen -t ed25519
```

Mit der Option `-C` erzeugen Sie ein Schlüsselpaar, der ein hinterlegten Kommentar enthält. Zum Beispiel die E-Mail-Adresse oder Nutzername und Rechnername (nutzer@rechner):

```
ssh-keygen -t ed25519 -C 'ich@laptop.local'
```

### Öffentlichen Schlüssel auf das Gerät kopieren

Sie müssen als nächstes Ihren öffentlichen Schlüssel auf Ihren Gerät übertragen. Es gibt mehrere Arten dies zu tun. Dies sind ein paar Möglichkeiten:

* Verbinden Sie Ihren UBports Gerät mit dem PC mit den USB-Kabel. Dann kopieren Sie die Datei mithilfe des Dateimanagers.

* oder, übertragen Sie den Schlüssel übers Internet indem Sie es sich selbst e-mailen, oder durch den Upload auf Ihr Owncloud/Nextcloud, Web-server, etc.

* Sie können sich auch über [adb](https://docs.ubports.com/en/latest/userguide/advanceduse/adb.html) verbinden und es durch die Eingabe folgender Befehl kopieren (Manche Geräte können kein ADB unterstützen, schauen Sie in der Geräte-Seite um mehr zu erfahren.):

```
adb push ~/.ssh/id_rsa.pub /home/phablet/
```

### Das Gerät Konfigurieren

Jetzt haben Sie den öffentlichen Schlüssel auf dem UBports-Gerät. Nehmen wir an, es wurde als `/home/phablet/id_rsa.pub` gespeichert. Verwenden Sie die Terminal-App oder eine ADB Verbindung um folgende Schritte auf Ihren Gerät vorzunehmen:

```
mkdir /home/phablet/.ssh
chmod 700 /home/phablet/.ssh
cat /home/phablet/id_rsa.pub >> /home/phablet/.ssh/authorized_keys
chmod 600 /home/phablet/.ssh/authorized_keys
chown -R phablet:phablet /home/phablet/.ssh
```

Jetzt starten Sie den SSH server. Falls Sie ein Android basiertes Gerät verwenden:

```
sudo android-gadget-service enable ssh
```

Wenn Sie ein Linux basiertes Gerät (wie der PinePhone) verwenden:

```
sudo service ssh start
```

### Verbinden

Jetzt ist alles konfiguriert, Sie können `ssh` verwenden:

```
ssh phablet@ubuntu-phablet
```

oder, wenn es nicht funktioniert

```
ssh phablet@<ip-address>
```

Um die IP-Adresse Ihres UBports-Gerät herauszufinden, öffnen Sie die Terminal App auf Ihren Gerät und führen folgenden Befehl aus:

```
hostname -I
```

Die Ausgabe ist eine Liste von IP-Adressen getrennt durch Leerzeichen. Verwenden Sie die IP-Adresse die mit Ihren SubNetz übereinstimmt. Auf Ihren PC oder Laptop:

```
debian2:~/$ hostname -I
192.168.42.41 2001:982:89e9:1:bc6b:758:7ba2:c190
```

Auf dem Handy:

```
phablet@ubuntu-phablet:~$ hostname -I
10.55.74.177 192.168.42.52 2001:982:89e9:1:ef68:5f7c:3db4:c0d3
```

In diesen Fall verwenden Sie die zweite IP Adresse.
Jetzt können Sie auch `scp` und `sshfs` verwenden um Daten zu übertragen.

### Referenzen

Aus der offizielle UBports Dokumentation: [Shell access via SSH](https://docs.ubports.com/es/latest/userguide/advanceduse/ssh.html).

## Links

[UBports](https://ubports.com/)
[Unterstützte Geräte](https://ubports.com/de/supported-products).
[Alle Ports und deren Zustand](https://devices.ubuntu-touch.io/)

[Shell access via SSH](https://docs.ubports.com/es/latest/userguide/advanceduse/ssh.html).

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment

Tiago Simões Tomé <tiagotome95@gmail.com>

## License
The documentation in this project is licensed under the [Creative Commons Attribution-ShareAlike 4.0 license](https://choosealicense.com/licenses/cc-by-sa-4.0/), the source code content (also the source code included in the documentation) is licensed under the [GNU GPLv3 license](https://choosealicense.com/licenses/gpl-3.0/).
